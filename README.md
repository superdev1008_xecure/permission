**Permission** exposes a unified API to request permissions on iOS.

<p align="center">
    <a href="#usage">Usage</a> • <a href="#example">Example</a> • <a href="#installation">Installation</a> • <a href="#license">License</a>
</p>

## Usage

```swift
let permission: Permission = .contacts

print(permission.status) // .notDetermined

permission.request { status in
    switch status {
    case .authorized:    print("authorized")
    case .denied:        print("denied")
    case .disabled:      print("disabled")
    case .notDetermined: print("not determined")
    }
}
```

##### Denied and disabled alerts

When you first request a permission, a system alert is presented to the user.
If you request a permission that was denied/disabled, a `PermissionAlert` will be presented.
You might want to change the default `title`, `message`, `cancel` and `settings` text:

```swift
let alert = permission.deniedAlert // or permission.disabledAlert

alert.title    = "Please allow access to your contacts"
alert.message  = nil
alert.cancel   = "Cancel"
alert.settings = "Settings"
```

Set `permission.presentDeniedAlert = false` or `permission.presentDisabledAlert = false` if you don't want to present these alerts.

##### Pre-permission alerts

In order not to burn your only chance of displaying the system alert, you can present a **pre-permission alert**. See this [article](http://techcrunch.com/2014/04/04/the-right-way-to-ask-users-for-ios-permissions/) for more informations.

```swift
permission.presentPrePermissionAlert = true

let alert = permission.prePermissionAlert

alert.title   = "Let Foo Access Photos?"
alert.message = "This lets you choose which photos you want to add to your Foo profile"
alert.cancel  = "Not now"
alert.confirm = "Give Access"
```

The system alert will only be presented if the user taps "Give Access".

#### PermissionSet

Use a `PermissionSet` to check the status of a group of `Permission` and to react when a permission is requested.

```swift
let permissionSet = PermissionSet(.contacts, .camera, .microphone, .photos)
permissionSet.delegate = self

print(permissionSet.status) // .notDetermined

// ...

func permissionSet(permissionSet: PermissionSet, willRequestPermission permission: Permission) {
    print("Will request \(permission)")
}

func permissionSet(permissionSet: PermissionSet, didRequestPermission permission: Permission) {
    switch permissionSet.status {
    case .authorized:    print("all the permissions are granted")
    case .denied:        print("at least one permission is denied")
    case .disabled:      print("at least one permission is disabled")
    case .notDetermined: print("at least one permission is not determined")
    }
}
```

#### PermissionButton


A `PermissionButton` requests the permission when tapped and updates itself when its underlying permission status changes.

```swift
let button = PermissionButton(.photos)
```

`PermissionButton` is a subclass of `UIButton`. All the getters and setters of `UIButton` have their equivalent in `PermissionButton`.

```swift
button.setTitles([
    .authorized:    "Authorized",
    .denied:        "Denied",
    .disabled:      "Disabled",
    .notDetermined: "Not determined"
])

// button.setAttributedTitles
// button.setTitleColors
// button.setTitleShadowColors
// button.setImages
// button.setBackgroundImages
// etc.
```


## Example

```swift
class PermissionsViewController: UIViewController, PermissionSetDelegate {

    override func viewDidLoad() {
        super.viewDidLoad()

        let label = UILabel()

        let contacts   = PermissionButton(.contacts)
        let camera     = PermissionButton(.camera)
        let microphone = PermissionButton(.microphone)
        let photos     = PermissionButton(.photos)

        contacts.setTitles([
            .notDetermined: "Contacts - NotDetermined"
            .authorized:    "Contacts - Authorized",
            .denied:        "Contacts - Denied"
        ])

        contacts.setTitleColors([
            .notDetermined: .black,
            .authorized:    .green,
            .denied:        .red
        ])

        // ...

        let permissionSet = PermissionSet(contacts, camera, microphone, photos)

        permissionSet.delegate = self

        label.text = String(describing: permissionSet.status)

        for subview in [label, contacts, camera, microphone, photos] {
            view.addSubview(subview)
        }
    }

    func permissionSet(permissionSet: PermissionSet, didRequestPermission permission: Permission) {
        label.text = String(permissionSet.status)
    }
}
```

<img align="center" src="https://raw.githubusercontent.com/delba/Permission/assets/permission.gif" />

## Installation

### Carthage

You can install Carthage with [Homebrew](http://brew.sh/) using the following command:

```bash
$ brew update
$ brew install carthage
```

To integrate Permission into your Xcode project using Carthage, specify it in your `Cartfile`:

```ogdl
github "delba/Permission"
```

##### Configuration

Due to Apple's new policy regarding permission access, binaries may be rejected due to a perceived attempt
to access privacy-sensitive data without a usage key, and then further rejected for not actually requesting
permissions.

As a workaround, you can provide custom build flags _before_ building the dynamic framework to only compile
with permissions you request. This is done by adding a configuration file named `PermissionConfiguration.xcconfig`
to the root of your project. For convenience, you can use
`PermissionConfiguration.xcconfig` in the `Permission/` repo directory. Just comment out the permissions
you want to use, and compile the framework.

To compile with only notifications and photos permissions:
```
PERMISSION_BLUETOOTH         = // PERMISSION_BLUETOOTH
PERMISSION_CAMERA            = PERMISSION_CAMERA
PERMISSION_CONTACTS          = // PERMISSION_CONTACTS
PERMISSION_EVENTS            = // PERMISSION_EVENTS
PERMISSION_LOCATION          = // PERMISSION_LOCATION
PERMISSION_MICROPHONE        = // PERMISSION_MICROPHONE
PERMISSION_MOTION            = // PERMISSION_MOTION
PERMISSION_NOTIFICATIONS     = PERMISSION_NOTIFICATIONS
PERMISSION_PHOTOS            = // PERMISSION_PHOTOS
PERMISSION_REMINDERS         = // PERMISSION_REMINDERS
PERMISSION_SPEECH_RECOGNIZER = // PERMISSION_SPEECH_RECOGNIZER
PERMISSION_MEDIA_LIBRARY     = // PERMISSION_MEDIA_LIBRARY

// Do not modify this line. Instead, remove comments above as needed to enable the categories your app uses.
PERMISSION_FLAGS= $(PERMISSION_BLUETOOTH) $(PERMISSION_CAMERA) $(PERMISSION_CONTACTS) $(PERMISSION_EVENTS) $(PERMISSION_LOCATION) $(PERMISSION_MICROPHONE) $(PERMISSION_MOTION) $(PERMISSION_NOTIFICATIONS) $(PERMISSION_PHOTOS) $(PERMISSION_REMINDERS) $(PERMISSION_SPEECH_RECOGNIZER) $(PERMISSION_MEDIA_LIBRARY)

SWIFT_ACTIVE_COMPILATION_CONDITIONS = $(inherited) $(PERMISSION_FLAGS)
```

### Cocoapods

[CocoaPods](http://cocoapods.org) is a dependency manager for Cocoa projects.

You can install it with the following command:

```bash
$ gem install cocoapods
```

To integrate Permission into your Xcode project using CocoaPods, specify it in your `Podfile`. Due to Apple's new policy regarding permission access you need to specifically define what kind of permissions you want to access using subspecs. For example if you want to access the Camera and the Notifications you define the following:

```ruby
use_frameworks!

pod 'Permission/Camera'
pod 'Permission/Notifications'
```

Please see `Permission.podspec` for more information about which subspecs are available.
